# 8.2 - Concurrency Problems - Session 5

## 1

Cuatro personas están sentados en una mesa jugando dominó, se van turnando para tirar una ficha o 
pasar si no pueden jugar en el sentido contrario a las manecillas del reloj. Represente a los cuatro 
jugadores con procesos desde P(0) hasta P(3) y como sincronizaría los turnos usando un arreglo de semáforos.


[Solution](https://docs.google.com/spreadsheets/d/1VWgsjUQPmHv8Ew2UKjnUjnXw3F0e4_rD2J4iYs_DuKM/edit?usp=sharing)


## 2

Considere el ejemplo del productor consumidor que se muestra a continuación

```
#define TAM_BUFFER 10
Semaphore s = 1;
Semaphore n = 0;
Semaphore e = TAM_BUFFER;

productor()
{
    while(TRUE)
    {
        producir();
        wait(e);
        wait(s);
        añadir();
        signal(s);
        signal(n);
    }
}

consumidor()
{
    while(TRUE)
    {
        wait(n);
        wait(s);
        tomar();
        signal(s);
        signal(e);
        consumir();
    }
}
```

¿Cambiaría el significado del programa si se intercambian las siguientes sentencias?

```
    wait(e); wait(s)
    signal(s); signal(n)
    wait(n); wait(s)
    signal(s); signal(e)
```

## 3

De ser posible implementar semáforos generales por medio de semáforos binarios. Se pueden usar
las operaciones WaitB y SignalB y dos semáforos binarios espera y exmut. Considere lo siguiente:

```
struct strsemáforo {
     int contador;
     BinSemaphore espera=0;
};


typedef struct strsemáforo * semáforo;
BinSemaphore exmut=1;

void Wait(semáforo s)
{
   WaitB(exmut);
   s->contador--;
   if(s->contador!=0)
   {
        SignalB(exmut);
        WaitB(s->espera);
    }
    else
        SignalB(exmut);
}

void Signal(semáforo s)
{
   WaitB(exmut);
   s->contador++;
   if(s->contador!=0)
      SignalB(s->espera);
   SinalB(exmut);
}
```


Inicialmente, `s->contador` tiene el valor deseado por el semáforo. Cada operación Wait disminuye 
`s->contador` y cada operación Signal lo incrementa. El semáforo binario exmut iniciado en 1, asegura
que hay exclusión mutua en las actualizaciones de s->contador. El semáforo binario s->espera, iniciado
a 0, se usa para suspender procesos.

De acuerdo a las especificaciones anteriores, muestren si la implementación de semáforos enteros a partir
de semáforos binarios es correcta, y si no lo es indique en dónde es incorrecto, proponga la corrección.

[Solution](https://gitlab.com/lsandov1/ITESO-FSO/-/commit/a918a9a428c6e0add29a486fac20ba7b318c3db9)

# 4

La siguiente solución intenta resolver el problema de permitir que `k` de `n` procesos puedan ejecutar 
concurrentemente su sección crítica usando para esto sólo semáforos binarios.

Muestre si es posible que haya más de K procesos en la sección crítica para:

```
k=2                 n=3
k=2                 n=4
```

y se podría concluir que no funciona para valores de `n` mayores.

Muestre que problemas puede presentar esta solución y plantee una solución que resuelva correctamente con semáforos binarios dicho problema.

```
    // Constantes

    const int K=2;
    const int N=4;

    // Variables
    int contador;
    int bloqueados=0;
    binarysem mutex=1;
    binarysem retardo=0;

    void proceso()
    {
 1:     wait(mutex);
 2:     if(contador==0)
        {
 3:         bloqueados++;
 4:         wait(retardo);
        }
        else
 5:         contador--;
 6:     signal(mutex);

        // Sección crítica

 7:     wait(mutex);
 8:     if(bloqueados>0)
        {
 9:         signal(retardo);
10:         bloqueados--;
        }
11:     else
12:         contador++;
13:     signal(mutex);
    }

    void main()
    {
14:     contador=K;
        cobegin
        {
15:         proceso(); proceso(); proceso();proceso();
        }
    }
```

Suponga que el semáforo mutex no existe, la variable contador=1 y dos procesos ejecutan concurrentemente (paralelamente) las líneas de la 1 a la 6.

Suponga que el semáforo mutex si existe, la variable contador=1 y dos procesos ejecutan concurrentemente (paralelamente) las líneas de la 1 a la 6. ¿Qué pasa a diferencia del caso de la pregunta anterior?

¿Qué sucede si contador=0 y un proceso ejecuta las líneas de la 1 a la 4?, esto funcionaría correctamente?, proponga modificaciones
