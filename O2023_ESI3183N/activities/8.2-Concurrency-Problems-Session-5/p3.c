struct strsemáforo {
     int contador;
     BinSemaphore espera=0;
};


typedef struct strsemáforo * semáforo;
BinSemaphore exmut=1;

void Wait(semáforo s)
{
   WaitB(exmut);
   s->contador--;
   if(s->contador<0)
   {
        WaitB(s->espera);
    }
   SignalB(exmut);
}

void Signal(semáforo s)
{
   WaitB(exmut);
   s->contador++;
   if(s->contador<=0)
      SignalB(s->espera);
   SignalB(exmut);
}
