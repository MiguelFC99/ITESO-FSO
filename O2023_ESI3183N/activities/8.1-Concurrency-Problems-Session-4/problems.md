# 8.1 - Concurrency Problems - Session 4

## 1

Considerando las definiciones de semáforos tanto enteros como binarios, uno de los requisitos para que estos
funcionen es que la ejecución de las llamadas `waitsem` y `signalsem` se haga de manera atómica. 
Muestra con un ejemplo por qué pueden fallar si no se asegura la atomicidad en la ejecución de ambas funciones.
Por ejemplo:

1. Dos procesos/hilos ejecutan `waitsem` sin garantizar la atomicidad de `waitsem`
2. Un proceso ejecuta `waitsem` y otro `signalsem` sin garantizar la atomicidad de `waitsem` y `signalsem`
3. Dos procesos ejecutan `signalsem` sin garantizar la atomicidad la función `signalsem`.

[Solution](https://docs.google.com/spreadsheets/d/1pQnrBPaU4ALMcooBTwv7L-Bn1WpZ-TMR6qYRAx12GLA/edit?usp=sharing)

## 2

Compara las definiciones de semáforos enteros donde los semáforos enteros pueden tomar valores negativos y
aquellos donde no toman valores negativos ¿hay alguna diferencia en el efecto de las dos definiciones cuando
se utilizan para sincronizar procesos? Es decir, ¿es posible sustituir una definición por la otra sin alterar
el significado del programa?

[Solution](https://docs.google.com/spreadsheets/d/1pQnrBPaU4ALMcooBTwv7L-Bn1WpZ-TMR6qYRAx12GLA/edit?usp=sharing)

## 3

La solución correcta al problema del productor consumidor que se muestra a continuación le hace falta considerar que
el buffer es finito, es decir, que el tamaño del buffer es limitado, usando semáforos binarios has las modificaciones
y agrega los semáforos necesarios para que cuando haya N elementos en el buffer el productor se bloquee si quiere 
agregar más elementos hasta que el consumidor consuma un elemento del buffer y haya un espacio disponible. 

```
#define MAX 10

int m,n;
SemaphoreBin s; // 1
SemaphoreBin retraso; // 0
SemaphoreBin lleno; // 0

Productor()
{
    while(forever)
    {
        producir;
        if (n==MAX) waitB(lleno);
        waitB(s);
        añadir;
        n++;
        if (n==1) signalB(retraso);
        signalB(s);
    }
}

Consumidor()
{
    waitB(retraso);
    while(forever)
    {
        waitB(s);
        tomar;
        n--;
        m=n;
        signalB(lleno);
        signalB(s);
        consumir;
        if (m==0) waitB(retraso);
    }
}

main()
{
   n=0;
   initbsem(s,1);
   initbsem(retraso,0);
   initbsem(lleno,0);
   cobegin {
      Productor();
      Consumidor();
   }
}
```

[Solution](https://gitlab.com/lsandov1/ITESO-FSO/-/commit/02ed03a887f2f67704224bfd13c7c69c31114062)

## 4

Considere un programa concurrente con dos procesos P y Q, definidos a continuación. la impresión en pantalla de
A, B, C, D y E son sentencias arbitrarias atómicas (indivisibles). Supóngase que el programa principal  ejecuta
concurrentemente los procesos P y Q. Comprueba tu solución empleando semáforos en Linux.

```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

void imprime(char *s)
{
    usleep(rand()%100000);
    printf("%s\n",s);
    usleep(rand()%100000);
}

void P()
{
    imprime("A");
    imprime("B");
    imprime("C");
    exit(0);
}

void Q()
{
    imprime("D");
    imprime("E");
    exit(0);
}

int main()
{
    int p;

    srand(getpid());

    p=fork();
    if(p==0)
        P();

    p=fork();
    if(p==0)
        Q();

    wait(NULL);
    wait(NULL);
}
```

A. Cuántas combinaciones posibles hay en la intercalación de las sentencias A, B, C, D y E.

B. Usando semáforos sincronice P y Q de manera que se asegure que las sentencias A y B se ejecutan antes que D.

C. Usando semáforos haga que el proceso P ejecute siempre las sentencia B inmediatamente después que A sin que
cualquier sentencia de Q se ejecute intercalada entre ellas.

[Solution](https://docs.google.com/spreadsheets/d/1pQnrBPaU4ALMcooBTwv7L-Bn1WpZ-TMR6qYRAx12GLA/edit?usp=sharing)
