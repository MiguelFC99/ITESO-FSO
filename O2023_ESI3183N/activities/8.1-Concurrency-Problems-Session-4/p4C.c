#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include "semaphoresarr.h"

#define NUM_SEM 1
#define SEQUENCE 0

SEM_ID semarr;

void imprime(char *s)
{
    usleep(rand()%100000);
    printf("%s\n",s);
    usleep(rand()%100000);
}

void P()
{
    imprime("A");
    imprime("B");
    semsignal(semarr,SEQUENCE);
    imprime("C");
    exit(0);
}

void Q()
{
    semwait(semarr,SEQUENCE);
    imprime("D");
    imprime("E");
    exit(0);
}

int main()
{
    int p;
    semarr=createsemarray((key_t) 0x4008,1);
    initsem(semarr,SEQUENCE,0);

    srand(getpid());

    p=fork();
    if(p==0)
        P();

    p=fork();
    if(p==0)
        Q();

    wait(NULL);
    wait(NULL);
}
