all: CalcularPIP1 regla_TrapecioP1

CalcularPIP1: CalcularPIP1.c
	gcc -o CalcularPIP1 CalcularPIP1.c -lm  -pthread

regla_TrapecioP1: regla_TrapecioP1.c
	gcc -o regla_TrapecioP1 regla_TrapecioP1.c -lm

clean:
	rm CalcularPIP1 regla_TrapecioP1
