/*
  Para compilar incluir la librería m (matemáticas)
  Ejemplo:
  gcc -o mercator_proc mercator_proc.c -lm
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include "semaphoresarr.h"

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

double *sums;
double x = 1.0;

double *res;

SEM_ID semarr;
enum {SEM_ARRANQUE,SEM_TERMINADO};  // Semáforos 0 y 2


double get_member(int n, double x)
{
  int i;
  double numerator = 1;
  for(i=0; i<n; i++ )
    numerator = numerator*x;
  if (n % 2 == 0)
    return ( - numerator / n );
  else
    return numerator/n;
}

void proc(int proc_num)
{
  int i;

  semwait(semarr,SEM_ARRANQUE);

  sums[proc_num] = 0;
  for(i=proc_num; i<SERIES_MEMBER_COUNT;i+=NPROCS)
    sums[proc_num] += get_member(i+1, x);
  
  semsignal(semarr,SEM_TERMINADO);

  exit(0);
}

void master_proc()
{
  int i;
  sleep(1);
  
  // start all producers
  for(i=0; i<NPROCS; i++) {
    semsignal(semarr,SEM_ARRANQUE);
  }

  // wait for producers to complete
  for(i=0; i<NPROCS; i++) {
    semwait(semarr,SEM_TERMINADO);
  }

    *res = 0;
  for(i=0; i<NPROCS; i++)
    *res += sums[i];
  exit(0);
}

int main()
{
  long long start_ts;
  long long stop_ts;
  long long elapsed_time;
  long lElapsedTime;
  struct timeval ts;
  int i;
  int p;
  int shmid;
  void *shmstart;

  // Creación del arreglo de semáforos
  semarr=createsemarray((key_t) 0x4008,2);

  initsem(semarr,SEM_ARRANQUE,0);
  initsem(semarr,SEM_TERMINADO,0);

  shmid = shmget(0x1234,(NPROCS+1)*sizeof(double),0666|IPC_CREAT);
  shmstart = shmat(shmid,NULL,0);
  sums = shmstart;

  res = shmstart + NPROCS * sizeof(double);

  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec; // Tiempo inicial
  for(i=0; i<NPROCS;i++)
    {
      p = fork();
      if(p==0)
	proc(i);
    }
  p = fork();
  if(p==0)
    master_proc();

  printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n",SERIES_MEMBER_COUNT);
  printf("El valor del argumento x es %f\n", (double)x);

  for(int i=0;i<NPROCS+1;i++)
    wait(NULL);

  gettimeofday(&ts, NULL);
  stop_ts = ts.tv_sec; // Tiempo final
  elapsed_time = stop_ts - start_ts;

  printf("Tiempo = %lld segundos\n", elapsed_time);
  printf("El resultado es %10.8f\n", *res);
  printf("Llamando a la función ln(1 + %f) = %10.8f\n",x, log(1+x));

  shmdt(shmstart);
  shmctl(shmid,IPC_RMID,NULL);
}
