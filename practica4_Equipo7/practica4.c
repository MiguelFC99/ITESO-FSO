#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/msg.h>
#include <sys/ipc.h>

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

double x = 1.0;

long long start_ts;
long long stop_ts;
long long elapsed_time;
struct timeval ts;

struct message {
    long mtype;
    double value;
};

double get_member(int n, double x)
{
	int i;
	double numerator = 1;
	for(i=0; i<n; i++ )
    	numerator = numerator*x;
	if (n % 2 == 0)
    	return ( - numerator / n );
	else
    	return numerator/n;
}

void proc(int proc_num, int msgid)
{
    struct message msg;
    msg.mtype = proc_num + 1;
    //procesos esclavos esperan a que MasterProc envie mensajes de inicio
    while (msgrcv(msgid, &msg, sizeof(double), msg.mtype, 0) == -1); 
    
    double partial_sum = 0;
    for (int i = proc_num; i < SERIES_MEMBER_COUNT; i += NPROCS) {
        partial_sum += get_member(i + 1, x);
    }
    
    //Se guarda el resultado parcial y se envia a la cola de mensajes
    msg.value = partial_sum;
    if (msgsnd(msgid, &msg, sizeof(double), 0) == -1) {
        perror("Error al enviar el resultado a la cola de mensajes!");
        exit(1);
    }
    
    exit(0);
}

void master_proc(int msgid)
{
    double total_sum = 0;
    for (int i = 0; i < NPROCS; i++) {
        struct message msg;
        msg.mtype = i + 1;
        msg.value = 0;
        //Master envia msg para que los procesos esclavos trabajen
        if (msgsnd(msgid, &msg, sizeof(double), 0) == -1) {
            perror("Error al enviar los mensajes para que inicien los procesos esclavos");
            exit(1);
        }
    }
	//Master recibe los mensajes con los resultados parciales y lo suma a total_sum
    for (int i = 0; i < NPROCS; i++) {
        struct message msg;
        if (msgrcv(msgid, &msg, sizeof(double), i + 1, 0) == -1) {
            perror("Error al recibir resultados de los procesos esclavos");
            exit(1);
        }
        total_sum += msg.value;
    }
	
	gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;

    
    printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n", SERIES_MEMBER_COUNT);
    printf("El valor del argumento x es %f\n", (double)x);
    printf("Tiempo = %lld segundos\n", elapsed_time);
    printf("El resultado es %10.8f\n", total_sum);
    printf("Llamando a la función ln(1 + %f) = %10.8f\n", x, log(1 + x));
    
    exit(0);
}

int main()
{
    int msgid = msgget(IPC_PRIVATE, IPC_CREAT | 0666);
    
    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial
    
    for (int i = 0; i < NPROCS; i++) {
        pid_t p = fork();
        if (p == 0) {
            proc(i, msgid);
        }
    }

    pid_t p = fork();
    if (p == 0) {
        master_proc(msgid);
    }

    for (int i = 0; i < NPROCS + 1; i++) {
        wait(NULL);
    }
	//liberamos cola de mensajes
    msgctl(msgid, IPC_RMID, NULL);
    
    return 0;
}
